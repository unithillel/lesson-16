<?php

namespace App;

use App\Classes\User;
use App\Classes\Admin;
use App\Classes\Module\Entity;
use App\Classes\Entity as AppEntity;

spl_autoload_register(__NAMESPACE__.'\fakeAutoload');
spl_autoload_register(__NAMESPACE__.'\app_class_autoload');


echo Entity::sayHi();
echo '<br>';
echo AppEntity::sayHi();
echo '<br>';
echo User::sayHi();
echo '<br>';
echo Admin::sayHi();
echo "<br>";



/*function __autoload($className){
    $filePath = str_replace('App\\','', $className);
    $filePath = str_replace('\\','/', $filePath).'.php';
    require_once $filePath;
}*/

function fakeAutoload($className){

}

function app_class_autoload($className){
    $filePath = str_replace('App\\','', $className);
    $filePath = str_replace('\\',DIRECTORY_SEPARATOR, $filePath).'.php';
    require_once $filePath;
}







